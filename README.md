# ResuMan

A Django application for managing portfolios, resumes, blogs and other information centered websites.

[Styleguide](https://phalt.github.io/django-api-domains/styleguide)

### Status

Provides RESTful endpoints for

- [x] Projects
- [ ] Journals
- [x] Bios
- [x] Contact pages

Provides metrics for

- [ ] Site visits
- [ ] API calls

### TODO

- [ ] Implement robust testing
- [ ] Apply Domain Driven Design via refactoring
- [ ] Refactor CI/CD pipelines from GitHub

## Projects

Projects are a domain to showcase your work.
They have a title, date, description, display photo, tags and most importantly a report,
which has headings, subheadings, paragraphs and yes, photos.

#### Objectives

- Streamline showcasing your projects

## Journals

Journals are a domain to document your process.
They are essentially blogs.
They have entries which have a date, author, title and body.

```plantuml
package JournalDomain <<Cloud>> {
  class Entry {
    author
    title
    date
    body
    updateData()
  }
}
```

#### Objectives

- Streamline documenting your process

## Bios

Bios are a domain to introduce yourself.
Add a describing text about yourself along with your resume.
Spice it up with a headshot and some hyperlinks to your social media.

#### Objectives

- Streamline adding and updating your resume.

## Contact

Contact is a domain to.. you guessed it: contact.

```plantuml
package ContactDomain <<Cloud>> {

  class Request {
    name: str
    email: str
    body: str
  }
}
```

#### Objectives

- Set up a pipeline from consuming a POST request to delivering data via email.

## Testing

> 1. Test behavior not implementation details
> 2. Strive for TDD
