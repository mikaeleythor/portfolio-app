#!/bin/bash

# NOTE: Because user is not root
export PATH=../.local/bin:$PATH

# NOTE: Don't need to migrate everytime I start during dev
# python3 manage.py makemigrations projects
# python3 manage.py migrate

printf "\n\nCREATING SUPERUSER\n"
python3 manage.py createsuperuser \
    --noinput \
    --username "$DJANGO_SUPERUSER_USERNAME" \
    --email "$DJANGO_SUPERUSER_EMAIL"

printf "\n\nGENERATING SCHEMAS\n"
python3 manage.py generateschema > openapi-schema.yml

printf "\n\nCOLLECTING STATIC FILES\n"
python3 manage.py collectstatic --noinput

printf "\n\nSTARTING CELERY WORKER\n"
python3 -m celery -A portfolio worker -l INFO --detach

# WARNING: Only use in development!
printf "\n\nSTARTING SERVER IN DEV MODE\n"
gunicorn portfolio.wsgi --bind 0.0.0.0:8000 --reload
