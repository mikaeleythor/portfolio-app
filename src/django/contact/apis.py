from rest_framework.decorators import api_view
from rest_framework.response import Response
from contact import serializers, services


@api_view(['POST'])
def contact(request):
    """
    Post endpoint for contact requests which saves data
    and triggers async email tasks
    """
    # NOTE: PersonSerializer parses email and name fields
    person_serializer = serializers.PersonSerializer(data=request.data)
    # NOTE: RequestSerializer parses all the other fields
    request_serializer = serializers.RequestSerializer(data=request.data)
    if (
        person_serializer.is_valid(raise_exception=True) &
        request_serializer.is_valid(raise_exception=True)
    ):
        person_instance, created = services.save_contact_request(
            person_serializer.validated_data, request_serializer.validated_data
        )
        # TODO: Only send emails if created
        # if created:
        # send_mail(person_instance)
        services.send_mail(person_instance)
        return Response(status=200, data="Message schlonged")
    return Response(status=400, data={
        'person': str(person_serializer.errors),
        'request': str(request_serializer.errors)
    })
