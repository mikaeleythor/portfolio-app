from rest_framework import serializers
from contact.models import Request


class PersonSerializer(serializers.Serializer):

    name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)


class RequestSerializer(serializers.ModelSerializer):

    class Meta:
        model = Request
        # NOTE: Implementation required in API
        exclude = ['requester']
