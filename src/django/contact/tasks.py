import os
from portfolio.celery import app
from django.core.mail import EmailMessage


@app.task
def mail_task(title, body, recipient):
    msg = EmailMessage(title,
                       body,
                       os.environ.get('EMAIL_HOST_USER'),
                       [recipient])
    msg.content_subtype = 'html'
    # WARNING: Review this before production
    return msg.send(fail_silently=False)
