## Contact

- [x] Write class diagram
- [x] Write functional requirements
- [x] Write sequence diagram
- [x] Set up celery
  - [x] Set up redis
  - [x] Create test task
- [x] Set up email credentials
- [x] Write endpoint schema
- [x] Implement models
- [x] Implement apis
- [x] Implement services
- [x] Implement serializers

```plantuml

package ContactDomain <<Cloud>> {

  class Request {
    requester: Person
    title: str
    body: str
    date: str
  }

  class Person {
    name: str
    email: str
  }

  Request }o--|| Person
}
```

## Functional requirements

| #   | Description                                                                                        | Priority |
| --- | -------------------------------------------------------------------------------------------------- | -------- |
| 1   | Anyone can send a request with a title, body                                                       | A        |
| 2   | A requester must provide a valid email and a name                                                  | A        |
| 3   | A request must contain a valid date                                                                | A        |
| 4   | If an email address is registered en the backend, a request triggers an email sent to that address | A        |

## Sequence diagram

```plantuml
title posting a request
entity client
boundary api
control service
database db
boundary celery

client -> api: post request data
activate api #Blue
  api -> api: validate request
  api -> service: create request
  activate service #Blue
    service --> db: write data
    service --> celery: send mail
    return ok
  deactivate service
  api --> client: ok

deactivate api


```
