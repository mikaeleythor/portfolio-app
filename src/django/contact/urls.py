from django.urls import path
from contact.apis import contact


urlpatterns = [
    path('', contact, name='contact'),
]
