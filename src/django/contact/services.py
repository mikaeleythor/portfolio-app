from contact import tasks, models


def save_contact_request(validated_person_data, validated_request_data):
    person_instance, created = models.Person.objects.get_or_create(
        validated_person_data.get('email'))
    models.Request.objects.create(
        requester=person_instance, **validated_request_data)
    return person_instance, created


def send_mail(person):
    tasks.mail_task.delay(
        title='Thank you for your request',
        body=f'Dear {person.name}\
        Thank you for your request, I will reach out soon.\
        \
        Regards\
        Ligma Shrigma',
        recipient=person.email
    )
