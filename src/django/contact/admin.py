from django.contrib import admin
from contact.models import Request, Person


admin.site.register(Request)
admin.site.register(Person)
