from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=50, blank=False, null=False)
    email = models.EmailField(blank=False, null=False, unique=True)

    def __str__(self):
        return self.name


class Request(models.Model):
    title = models.CharField(max_length=50, null=False, blank=False)
    body = models.TextField(null=False, blank=False)
    date = models.DateTimeField(auto_now_add=True)
    requester = models.ForeignKey(Person, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
