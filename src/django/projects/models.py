import uuid
from django.db import models
from django.utils.text import slugify
from django.core.exceptions import ValidationError


def project_photo_directory_path(instance, filename):
    return f'projects/{instance.project.slug}/{filename}'


class Project(models.Model):
    """
    The project model interfaces project data on the shallowest level
    """

    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    title = models.CharField(max_length=50)
    date = models.DateField(blank=True)
    description = models.TextField()
    published = models.BooleanField(default=False)
    slug = models.SlugField(null=True, unique=True, blank=True)

    tags = models.JSONField()

    def __str__(self) -> str:
        return f'{self.slug}'

    def _get_unique_slug(self):
        slug = slugify(self.title)
        unique_slug = slug
        num = 1
        while Project.objects.filter(slug=unique_slug).exists():
            unique_slug = f'{slug}-{num}'
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-date']


class DisplayPhoto(models.Model):
    """
    Display photos for projects. Alt usage enforced.
    """
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)

    project = models.ForeignKey(
        Project,
        related_name='photo',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    upload = models.ImageField(
        upload_to=project_photo_directory_path)

    alt = models.CharField(max_length=50, blank=False)

# -------------------------------------- #
# -----------REPORT MODELS-------------- #
# -------------------------------------- #


class Item(models.Model):
    """
    A broadly defined model for either Markdown objects with text-content or
    photo objects with upload and alt fields. Id and Num fields are required
    for querying and sorting, respectively.

    Custom validation in serializer recommended for distinguishing usage.
    """

    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)

    project = models.ForeignKey(
        Project,
        related_name='report',
        on_delete=models.CASCADE,
    )

    title = models.CharField(blank=True, max_length=50)
    num = models.FloatField(null=False, blank=False)
    upload = models.ImageField(upload_to=project_photo_directory_path)
    alt = models.CharField(max_length=50, blank=True)
    content = models.TextField(blank=True)

    def clean(self):
        photo_fields = [self.upload, self.alt]
        markdown_fields = [self.title, self.content]
        if any(photo_fields is None) or any(markdown_fields is None):
            raise ValidationError(
                "Photo fields are upload & alt\n" +
                "Markdown fields are title & content"
            )
