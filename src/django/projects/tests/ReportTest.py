from django.core.files import File
from .ProjectTest import ProjectTest
from projects.apis import ReportViewSet
import requests
from io import BytesIO


def fetch_image_from_url(url='https://picsum.photos/200/300?random=1'):
    """Returns File object containing an image fetched from given url"""
    response = requests.get(url)
    if response.status_code == 200:
        file_name = url.split('/')[-1]
        image = File(BytesIO(response.content), name=file_name+'.png')
        return {'upload': image, 'alt': 'test photo', 'num': 2.0}
    return None


class ReportTest(ProjectTest):

    #################################################
    # ############# STATIC METHODS ##################
    #################################################

    @staticmethod
    def create_markdown_item(num):
        return {
            "num": num,
            "title": "yummerz",
            "content": "sexy boy",
        }

    #################################################
    # ############# HELPER METHODS ##################
    #################################################

    def _post_project(self):
        return super()._post_project()

    def _put_project(self, projectID):
        return super()._put_project(projectID)

    def _post_report(self, REPORT_DATA, projectID):
        request = self.factory.post(
            path=f'{self.endpoint}{projectID}/reports/',
            data=REPORT_DATA
        )
        request.user = self.user

        response = ReportViewSet.as_view({'post': 'create'})(
            request, project_pk=projectID)

        return response

    def _get_report(self, projectID, user):
        request = self.factory.get(
            path=f'{self.endpoint}{projectID}/reports/',
        )
        request.user = user

        response = ReportViewSet.as_view({'get': 'list'})(
            request, project_pk=projectID)

        return response

    def _report_is_sorted(self, report):
        for i in range(1, len(report)):
            if report[i-1]['num'] > report[i]['num']:
                return False
        return True

    def _post_report_items(self):
        response = self._post_project()
        projectID = response.data['id']
        for i in range(1, 5):
            self._post_report(
                self.create_markdown_item(float(i)), projectID
            )
        return projectID

    def _update_item(self, ITEM, projectID, itemID):
        request = self.factory.put(
            path=f'{self.endpoint}{projectID}/reports/{itemID}/',
            data=ITEM
        )
        request.user = self.user

        response = ReportViewSet.as_view({'put': 'update'})(
            request, project_pk=projectID, pk=itemID)

        return response

    def _delete_report_item(self, projectID, itemID):
        request = self.factory.delete(
            path=f'{self.endpoint}{projectID}/reports/{itemID}/',
        )
        request.user = self.user

        response = ReportViewSet.as_view({'delete': 'destroy'})(
            request, project_pk=projectID, pk=itemID)

        return response

    #################################################
    # ################### TESTS #####################
    #################################################

    def setUp(self):
        super().setUp()

    def testPostReportItem(self):
        """
        USE CASE 4

        User can post project report
        """
        response = self._post_project()
        projectID = response.data['id']
        REPORT_DATA = fetch_image_from_url()
        response = self._post_report(REPORT_DATA, projectID)
        self.assertEqual(response.status_code, 201,
                         f"Status code should be 201: {response.data}")

    def testUpdateReportItem(self):
        """
        USE CASE 5

        User can update project report attributes
        """
        projectID = self._post_report_items()
        response = self._get_report(projectID, self.user)
        item = response.data[0]
        self.assertEqual(item['num'], 1.0,
                         "First item in this array should have num 1.0")
        item['num'] = 18.0
        item['upload'] = ''
        itemID = item.pop('id')
        item.pop('project')
        response = self._update_item(item, projectID, itemID)
        response = self._get_report(projectID, self.user)
        self.assertEqual(response.data[-1]['num'], 18.0,
                         "updated value should be last")

    def testListPublishedReportItems(self):
        """
        USE CASE 8

        Anyone can view a report of a published project
        """
        response = self._post_project()
        projectID = response.data['id']
        self._put_project(projectID)
        REPORT_DATA = self.create_markdown_item(2.0)
        self._post_report(REPORT_DATA, projectID)
        response = self._get_report(projectID, self.anonymous)
        self.assertEqual(response.status_code, 200,
                         f"Status code should be 200: {response.data}")
        self.assertEqual(len(response.data), 1,
                         "Only one report item has been posted")

    def testListReportItems(self):
        """
        USE CASE 9

        User can view any project report in detail
        """
        response = self._post_project()
        projectID = response.data['id']
        REPORT_DATA = self.create_markdown_item(2.0)
        self._post_report(REPORT_DATA, projectID)
        response = self._get_report(projectID, self.user)
        self.assertEqual(response.status_code, 200,
                         f"Status code should be 200: {response.data}")
        self.assertEqual(len(response.data), 1,
                         "Only one report item has been posted")

    def testDestroyReportItem(self):
        """
        USE CASE 11

        User can delete a project report item
        """
        response = self._post_project()
        projectID = response.data['id']
        REPORT_DATA = self.create_markdown_item(2.0)
        self._post_report(REPORT_DATA, projectID)
        response = self._get_report(projectID, self.user)
        self.assertEqual(response.status_code, 200,
                         f"Status code should be 200: {response.data}")
        self.assertEqual(len(response.data), 1,
                         "Only one report item has been posted")

        projectID = self._post_report_items()
        response = self._get_report(projectID, self.user)
        len1 = len(response.data)
        itemID = response.data[0]['id']
        self._delete_report_item(projectID, itemID)
        response = self._get_report(projectID, self.user)
        self.assertEqual(len(response.data), len1-1,
                         "there should be one fewer report items now")

    def testReportItemSort(self):
        """
        USE CASE 12

        Project report items are returned in sorted order (by num)
        """
        response = self._post_project()
        projectID = response.data['id']
        for i in range(1, 5):
            self._post_report(
                self.create_markdown_item(float(i)), projectID
            )
        response = self._get_report(projectID, self.user)
        self.assertTrue(self._report_is_sorted(response.data))
