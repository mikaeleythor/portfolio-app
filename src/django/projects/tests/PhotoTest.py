from django.core.files import File
from .ProjectTest import ProjectTest
from projects.apis import PhotoViewSet
import requests
from io import BytesIO


def fetch_image_from_url(url='https://picsum.photos/200/300?random=1'):
    """Returns File object containing an image fetched from given url"""
    response = requests.get(url)
    if response.status_code == 200:
        file_name = url.split('/')[-1]
        image = File(BytesIO(response.content), name=file_name)
        return {'upload': image, 'alt': 'test photo'}
    return None


class PhotoTest(ProjectTest):

    #################################################
    # ############# HELPER METHODS ##################
    #################################################

    def _post_project(self):
        return super()._post_project()

    def _update_project(self, projectID):
        return super()._put_project(projectID)

    def _get_project_photo(self, projectID, photoID, user):
        request = self.factory.get(
            path=f'{self.endpoint}{projectID}/photos/{photoID}/',
        )
        request.user = user

        response = PhotoViewSet.as_view({'get': 'retrieve'})(
            request, project_pk=projectID, pk=photoID)

        return response

    def _post_project_photo(self, PHOTO_OBJECT, projectID):
        request = self.factory.post(
            path=f'{self.endpoint}{projectID}/photos/',
            data=PHOTO_OBJECT
        )
        request.user = self.user

        response = PhotoViewSet.as_view({'post': 'create'})(
            request, project_pk=projectID)

        return response

    #################################################
    # ################### TESTS #####################
    #################################################

    def setUp(self):
        super().setUp()

    def testPostProjectPhoto(self):
        """
        USE CASE 13

        User can post a project photo
        """
        response = self._post_project()
        projectID = response.data['id']
        PHOTO_OBJECT = fetch_image_from_url()
        response = self._post_project_photo(PHOTO_OBJECT, projectID)
        self.assertEqual(response.status_code, 201,
                         f"Status code should be 201: {response.data}")

    def testGetRenderedProjectPhoto(self):
        """
        USE CASE 14

        Anyone can retrieve a rendered project photo for a published project
        """
        response = self._post_project()
        projectID = response.data['id']
        PHOTO_OBJECT = fetch_image_from_url()
        response = self._post_project_photo(PHOTO_OBJECT, projectID)
        photoID = response.data
        response = self._get_project_photo(projectID, photoID, self.anonymous)
