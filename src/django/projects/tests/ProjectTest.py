from django.contrib.auth.models import User, AnonymousUser
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from projects.apis import ProjectViewSet
from create_user import create_test_user

#################################################
# ############### STATIC DATA ###################
#################################################

POST_DATA = {
    "title": "Test",
    "date": "1997-01-02",
    "description": "A test project",
    "tags": ["django", "python", "devops"]
}

PUT_DATA = {
    "title": "Test",
    "date": "1997-01-02",
    "description": "A test project",
    "published": True,
    "tags": ["django", "python", "devops"]
}


class ProjectTest(TestCase):

    #################################################
    # ############# HELPER METHODS ##################
    #################################################

    def _post_project(self):
        request = self.factory.post(
            path=self.endpoint,
            data=POST_DATA,
            format='json'
        )
        request.user = self.user

        response = ProjectViewSet.as_view({'post': 'create'})(request)

        return response

    def _put_project(self, projectID: str):
        request = self.factory.put(
            path=f'{self.endpoint}{projectID}/',
            data=PUT_DATA,
            format='json'
        )
        request.user = self.user

        response = ProjectViewSet.as_view(
            {'put': 'update'})(request, pk=projectID)

        return response

    def _get_projects(self, user):
        request = self.factory.get(
            path=self.endpoint,
        )
        request.user = user

        response = ProjectViewSet.as_view({'get': 'list'})(request)

        return response

    #################################################
    # ################### TESTS #####################
    #################################################

    def setUp(self):
        create_test_user()
        self.factory = APIRequestFactory()
        self.user = User.objects.get(username='apitestuser')
        self.anonymous = AnonymousUser()
        self.endpoint = 'projects/'

    def testCreateProject(self):
        """
        USE CASE 1

        User can post a new project with a title, description, tags
        and optionally display photo, report, date, slug
        """
        response = self._post_project()
        # TEST: status code
        self.assertEqual(
            response.status_code, 201, "Status code should be 201"
        )
        # TEST: response body
        self.assertTrue(
            dict(response.data, **POST_DATA) == response.data
        )

    def testUpdateProject(self):
        """
        USE CASE 2

        User can choose to publish or unpublish existing project

        USE CASE 3

        User can update project attributes
        """
        projectID = self._post_project().data['id']
        response = self._put_project(projectID)
        # TEST: status code
        self.assertEqual(
            response.status_code, 202, "Status code should be 202"
        )
        # TEST: response body
        self.assertTrue(dict(response.data, **PUT_DATA) == response.data)

    def testGetAllProjects(self):
        """
        USE CASE 6

        User can get a list of all projects
        """
        self._post_project()
        response = self._get_projects(self.user)

        # TEST: status code
        self.assertEqual(response.status_code, 200,
                         "Status code should be 200")
        # TEST: response body
        self.assertEqual(len(response.data), 1,
                         "Only one project has been posted")
        # TEST: response body
        self.assertTrue(
            dict(response.data[0], **POST_DATA) == response.data[0])

    def testGetPublishedProjects(self):
        """
        USE CASE 7

        Anyone can get a list of published projects
        """
        self._post_project()
        response = self._get_projects(self.anonymous)

        # TEST: status code
        self.assertEqual(response.status_code, 200,
                         "Status code should be 200")
        # TEST: response body
        self.assertEqual(response.data, [],
                         "No projects have been posted")
