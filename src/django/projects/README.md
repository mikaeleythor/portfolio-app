## Project

The project app has six endpoints for the 14 use cases specified below

- [ ] Rewrite class diagram
- [ ] Revise endpoint table

```plantuml

package ProjectDomain <<Cloud>> {

  class Project {
    title: str
    date: obj
    description: str
    photo: str
    tags: list
    updateData()
    editReport()
  }

  class Report {
    editItems()
  }
  class Item {
    id
    type
    content
    updateData()
  }

  Project ||-|| Report
  Report ||-o{ Item

}
```

## Endpoints

| Endpoint                              | Methods          | Description                                                   |
| ------------------------------------- | ---------------- | ------------------------------------------------------------- |
| `projects`                            | `GET,POST`       | Create a single project or list all projects for UUID         |
| `projects/<project-id>`               | `GET,PUT,DELETE` | Retrieve, update or delete project                            |
| `projects/<project-id>/r`             | `GET,POST`       | Create a single report-item or list all report-items for UUID |
| `projects/<project-id>/r/<report-id>` | `PUT,DELETE`     | Update or delete report-item                                  |
| `projects/<project-id>/p`             | `POST`           | POST a single photo                                           |
| `projects/<project-id>/p/<photo-id>`  | `GET,PUT`        | Retrieve or update a single photo                             |

## Use cases

| #   | Description                                                                           | Priority |
| --- | ------------------------------------------------------------------------------------- | -------- |
| 1   | User can post a new project with a title, description, tags and optionally date, slug | A        |
| 2   | User can choose to publish or unpublish existing project                              | A        |
| 3   | User can update project attributes                                                    | A        |
| 4   | User can post a project report                                                        | A        |
| 5   | User can update report attributes                                                     | A        |
| 6   | User can get a list of all projects                                                   | A        |
| 7   | Anyone can get a list of published projects                                           | A        |
| 8   | Anyone can view a report of a published project in detail                             | A        |
| 9   | User can view a report of a any project in detail                                     | A        |
| 10  | User can delete a project                                                             | A        |
| 11  | User can delete a project report item                                                 | A        |
| 12  | Project report items are returned in sorted order                                     | A        |
| 13  | User can post a project photo                                                         | A        |
| 14  | Anyone can retrieve a rendered project photo                                          | A        |

## Getting all projects sequence diagram

```plantuml

title getting all projects
entity client
boundary api
control service
database db

note right of client: getting all projects
client->>api: GET projectList
activate api #Blue
	activate api #Blue
		api->>api: authenticate requester
	deactivate api
	api->>service: list projects
	activate service #Blue
		service-->>db: query
		service-->>api: projectList
	deactivate service
	activate api #Blue
	api->>api: deserialize projectList
	deactivate api
	api-->>client: OK + projectList
deactivate api

note right of client: getting a projectReport
client->>api: GET report
activate api #Blue
	note right of api: projectID is in URI
	activate api #Blue
		api->>api: authenticate requester
	deactivate api
	api->>service: retrieve report
	activate service #Blue
		service-->>db: get project-data
		activate service #Blue
		service->>service: reportID
		deactivate service
		service-->>db: get report-data
		activate service #Blue
		service->>service: itemIDs
		deactivate service
		loop for each itemID
			service-->>db: get item-data
		end
	service-->>api: items
	deactivate service
	activate api #Blue
	api->>api: deserialize items
	deactivate api
	api-->>client: OK + report
deactivate api
```
