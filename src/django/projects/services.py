from django.http import QueryDict
from django.db.models import QuerySet
from .models import Project, Item, DisplayPhoto

from uuid import UUID
from utils.BaseAttrService import BaseAttrService
from utils.BaseService import BaseService


class ProjectService(BaseService):
    model = Project

    def retrieve(self, pk) -> list:
        return super().retrieve(pk)

    def retrieve_if_published(self, pk) -> list:
        return super().retrieve_if_published(pk)

    def create(self, validated_data: QueryDict) -> UUID:
        return super().create(validated_data)

    def list_all(self) -> QuerySet:
        return super().list_all()

    def list_published(self) -> QuerySet:
        return super().list_published()

    def update(self, validated_data: QueryDict, pk) -> UUID:
        return super().update(validated_data, pk)


class PhotoService(BaseAttrService):

    base = Project
    model = DisplayPhoto
    basename = 'project'

    def create(self, bio_pk, validated_data: QueryDict) -> UUID:
        return super().create(bio_pk, validated_data)

    def retrieve(self, project_pk, pk):
        try:
            base_instance = self.base.objects.get(id=project_pk)
            instance = self.model.objects.get(**{self.basename: base_instance})
            return instance

        # TEST: Will there occur a KeyError here?
        except KeyError:
            raise self.base.DoesNotExist()

    def retrieve_if_published(self, project_pk, pk) -> QuerySet:
        try:
            base_instance = self.base.objects.get(
                id=project_pk, published=True)
            instance = self.model.objects.get(**{self.basename: base_instance})
            return instance

        except KeyError:
            raise self.base.DoesNotExist()


class ReportService(BaseAttrService):

    base = Project
    model = Item
    basename = 'project'

    def create(self, bio_pk, validated_data: QueryDict) -> UUID:
        return super().create(bio_pk, validated_data)

    def list(self, bio_pk=None) -> list:
        return super().list(bio_pk)

    def list_if_published(self, bio_pk=None, validated_data=None) -> list:
        return super().list_if_published(bio_pk, validated_data)

    def update(self, bio_pk, pk, validated_data):
        return super().update(bio_pk, pk, validated_data)

    def destroy(self, bio_pk, pk):
        return super().destroy(bio_pk, pk)
