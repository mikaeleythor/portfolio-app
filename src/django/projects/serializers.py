from rest_framework import serializers
from projects.models import Project, Item, DisplayPhoto


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = '__all__'


class ReportSerializer(serializers.Serializer):

    id = serializers.UUIDField(required=False)
    num = serializers.FloatField(required=True)
    title = serializers.CharField(required=False)
    upload = serializers.ImageField(required=False)
    alt = serializers.CharField(required=False)
    content = serializers.CharField(required=False)
    project = ProjectSerializer(read_only=True)

    # class Meta:
        # model = Item
        # fields = '__all__'


class PhotoSerializer(serializers.Serializer):

    class Meta:
        model = DisplayPhoto
        fields = '__all__'
