from django.urls import include, path
from .apis import ProjectViewSet, ReportViewSet, PhotoViewSet
from rest_framework_nested.routers import NestedSimpleRouter, SimpleRouter

router = SimpleRouter()
router.register(r'projects', ProjectViewSet, basename='projects')

report_router = NestedSimpleRouter(router, r'projects', lookup='project')
report_router.register(r'reports', ReportViewSet, basename='reports')

photo_router = NestedSimpleRouter(router, r'projects', lookup='project')
photo_router.register(r'photos', PhotoViewSet, basename='photos')

urlpatterns = [
    path('', include(router.urls)),
    path('', include(report_router.urls)),
    path('', include(photo_router.urls)),
]
