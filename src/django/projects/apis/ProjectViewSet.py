from rest_framework.parsers import JSONParser
from projects.serializers import ProjectSerializer
from projects.services import ProjectService
from utils.BaseViewSet import BaseViewSet


class ProjectViewSet(BaseViewSet):
    """
    ProjectViewSet combines the logic for views directly interfacing the
    Project model
    """
    service = ProjectService()
    serializer = ProjectSerializer
    parser_classes = [JSONParser]

    def create(self, request):
        return super().create(request)

    def list(self, request):
        return super().list(request)

    def update(self, request, pk=None):
        return super().update(request, pk)
