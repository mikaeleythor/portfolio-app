from rest_framework import status
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from django.core.exceptions import ObjectDoesNotExist
from renderers import JPEGRenderer, PNGRenderer
from utils.permissions import ReadOnly
from projects.serializers import PhotoSerializer
from projects.services import PhotoService
from utils.BaseAttrViewSet import BaseAttrViewSet


class PhotoViewSet(BaseAttrViewSet):

    service = PhotoService()
    serializer = PhotoSerializer
    permission_classes = [IsAuthenticated | ReadOnly]
    renderer_classes = [JPEGRenderer, PNGRenderer]
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def create(self, request, project_pk=None):
        return super().create(request, project_pk)

    def retrieve(self, request, pk=None, project_pk=None):
        """
        This view is for retrieving a rendered display-photo
        """
        try:
            if request.user.is_staff:
                queryset = self.service.retrieve(project_pk, pk)
                return Response(queryset, content_type='image/*')
            else:
                queryset = self.service.retrieve_if_published(project_pk, pk)
                return Response(queryset, content_type='image/*')

        except ObjectDoesNotExist as e:
            return Response(data=str(e), status=status.HTTP_204_NO_CONTENT)
