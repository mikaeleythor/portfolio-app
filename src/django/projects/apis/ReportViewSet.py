from rest_framework.parsers import JSONParser, MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from utils.permissions import ReadOnly
from projects.serializers import ReportSerializer
from projects.services import ReportService
from utils.BaseAttrViewSet import BaseAttrViewSet


class ReportViewSet(BaseAttrViewSet):

    service = ReportService()
    serializer = ReportSerializer
    permission_classes = [IsAuthenticated | ReadOnly]
    parser_classes = [JSONParser, MultiPartParser, FormParser]

    def create(self, request, project_pk=None):
        return super().create(request, project_pk)

    def list(self, request, project_pk=None):
        return super().list(request, project_pk)

    def update(self, request, project_pk=None, pk=None):
        return super().update(request, project_pk, pk)

    def destroy(self, request, project_pk=None, pk=None):
        return super().destroy(request, project_pk, pk)
