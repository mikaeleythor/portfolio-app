from django.contrib.auth import get_user_model


def create_test_user():
    """This function is used for testing in entrypoint scripts"""
    UserModel = get_user_model()

    if UserModel.objects.filter(username='apitestuser').exists():
        user = UserModel.objects.get(username='apitestuser')
        user.set_password('bi9pcCGLFQKqYg5')
    else:
        user = UserModel.objects.create_user(
            'apitestuser', password='bi9pcCGLFQKqYg5')

    user.is_superuser = True
    user.is_staff = True
    user.save()


if __name__ == "__main__":
    create_test_user()
