from django.urls import include, path
from .apis import BioViewSet, SkillsetViewSet
from rest_framework_nested.routers import NestedSimpleRouter, SimpleRouter

router = SimpleRouter()
router.register(r'bios', BioViewSet, basename='bios')

skillset_router = NestedSimpleRouter(router, r'bios', lookup='bio')
skillset_router.register(r'skillsets', SkillsetViewSet, basename='skillset')

# photo_router = NestedSimpleRouter(router, r'projects', lookup='project')
# photo_router.register(r'photos', PhotoViewSet, basename='photos')

urlpatterns = [
    path('', include(router.urls)),
    path('', include(skillset_router.urls)),
    # path('', include(photo_router.urls)),
]
