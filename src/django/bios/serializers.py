from rest_framework import serializers
from bios.models import Bio, Skill, Skillset, Education, Experience, Certificate


class SkillSerializer(serializers.ModelSerializer):

    class Meta:
        model = Skill
        fields = ['name']


class SkillsetSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(many=True)

    class Meta:
        model = Skillset
        fields = '__all__'


class EducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        fields = '__all__'


class ExperienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Experience
        fields = '__all__'


class CertificateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Certificate
        fields = '__all__'


class BioSerializer(serializers.ModelSerializer):
    experience = ExperienceSerializer(
        many=True, read_only=True, required=False)
    skillsets = SkillsetSerializer(many=True, read_only=True, required=False)
    education = EducationSerializer(many=True, read_only=True, required=False)
    certifications = CertificateSerializer(
        many=True, read_only=True, required=False)

    class Meta:
        model = Bio
        fields = '__all__'
