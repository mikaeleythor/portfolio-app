import uuid
from django.db import models
from django.utils.text import slugify


def person_photo_directory_path(instance, filename):
    return f'people/{instance.person.id}/{filename}'


class Experience(models.Model):
    company = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)

    def __str__(self) -> str:
        return f'{self.title} at {self.company}'

    class Meta:
        ordering = ['date_end', 'company']
        constraints = [
            models.UniqueConstraint(
                fields=['company', 'title', 'date_start', 'date_end'],
                name='unique_employment'
            )
        ]


class Education(models.Model):
    school = models.CharField(max_length=50)
    degree = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    date_start = models.DateField(blank=True, null=True)
    date_end = models.DateField(blank=True, null=True)

    def __str__(self) -> str:
        return f'{self.degree} at {self.school}'

    class Meta:
        ordering = ['date_end', 'school']
        constraints = [
            models.UniqueConstraint(
                fields=['school', 'degree', 'date_start', 'date_end'],
                name='unique_school_year')
        ]


# NOTE: ChatGPT wrote this model
class Certificate(models.Model):
    name = models.CharField(max_length=100)
    issuing_organization = models.CharField(max_length=100)
    date_earned = models.DateField()
    expiration_date = models.DateField(null=True, blank=True)
    credential_id = models.CharField(max_length=100, null=True, blank=True)
    url = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Skill(models.Model):
    name = models.CharField(max_length=50, unique=True)


class Skillset(models.Model):
    # WARNING: This should not be unique (languages, frameworks)
    category = models.CharField(max_length=50)

    # NOTE: This requires special handling in services
    skills = models.ManyToManyField(Skill)

    def __str__(self) -> str:
        return f'{self.category}'

    class Meta:
        ordering = ['category']


class Bio(models.Model):
    """
    The person model interfaces bios data on the shallowest level
    """
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    bio = models.TextField(blank=True)
    slug = models.SlugField(unique=True, blank=True)
    published = models.BooleanField(default=False)
    experience = models.ManyToManyField(Experience)
    skillsets = models.ManyToManyField(Skillset)
    education = models.ManyToManyField(Education)
    certifications = models.ManyToManyField(Certificate)

    def __str__(self) -> str:
        return f'{self.slug}'

    # Slugs are used as a fallback pk
    def _get_unique_slug(self):
        slug = slugify(self.name)
        unique_slug = slug
        num = 1
        while Bio.objects.filter(slug=unique_slug).exists():
            unique_slug = f'{slug}-{num}'
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['name']


class DisplayPhoto(models.Model):
    """
    Display photos for projects. Alt usage enforced.
    """
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    bio = models.ForeignKey(
        Bio,
        related_name='photo',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    upload = models.ImageField(
        upload_to=person_photo_directory_path)
    alt = models.CharField(max_length=50, blank=False)
