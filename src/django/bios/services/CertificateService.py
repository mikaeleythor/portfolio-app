from uuid import UUID
from bios.models import Bio, Certificate
from django.http import QueryDict
from utils.BaseAttrService import BaseAttrService


class CertificateService(BaseAttrService):

    base = Bio
    model = Certificate
    basename = 'bio'

    # NOTE: instance.bio_set.add() cannot be hardcoded into BaseAttrService
    def create(self, base_pk: UUID, validated_data: QueryDict) -> UUID:
        try:
            base_instance = self.base.objects.get(id=base_pk)
            instance = self.model.objects.create(**validated_data)

            # HACK: This is to add to ManyToManyField instance
            instance.bio_set.add(base_instance)
            return instance

        # TEST: Will there occur a KeyError here?
        except KeyError:
            raise self.base.DoesNotExist()

    def list(self, bio_pk=None) -> list:
        return super().list(bio_pk)

    def list_if_published(self, bio_pk=None, validated_data=None) -> list:
        return super().list_if_published(bio_pk, validated_data)

    def update(self, bio_pk, pk, validated_data):
        return super().update(bio_pk, pk, validated_data)

    def destroy(self, bio_pk, pk):
        return super().destroy(bio_pk, pk)
