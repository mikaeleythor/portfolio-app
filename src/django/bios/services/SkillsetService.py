from uuid import UUID
from bios.models import Bio, Skillset, Skill
from django.http import QueryDict
from utils.BaseAttrService import BaseAttrService


class SkillsetService(BaseAttrService):

    base = Bio
    model = Skillset
    basename = 'bio'

    # NOTE: instance.bio_set.add() cannot be hardcoded into BaseAttrService
    def create(self, bio_pk: UUID, validated_data: QueryDict) -> UUID:
        try:
            # NOTE: Skills need to be added through Skill model object
            skills = validated_data.pop('skills')

            # NOTE: Create new Skillset instance
            instance = self.model.objects.create(**validated_data)

            # NOTE: Create the Skill instances
            for skill in skills:
                # Returns tuple (instance, created_bool)
                skill_obj, crtd_bool = Skill.objects.get_or_create(**skill)
                # HACK: Add Skill to new Skillset instance
                skill_obj.skillset_set.add(instance)

            # NOTE: This raises DoesNotExist exception if not found
            base_instance = self.base.objects.get(id=bio_pk)

            # HACK: This is to add to ManyToManyField instance
            instance.bio_set.add(base_instance)
            return instance

        except Exception as e:
            raise e

    def list(self, bio_pk: UUID) -> list:
        return super().list(bio_pk)

    def list_if_published(self, bio_pk=None, validated_data=None):
        return super().list_if_published(bio_pk, validated_data)

    def update(self, bio_pk, pk, validated_data):
        try:
            skills = validated_data.pop('skills')
            # This raises Bio.DoesNotExist
            base_instance = self.base.objects.get(pk=bio_pk)
            # This raises Skillset.DoesNotExist
            instance = base_instance.skillsets.get(pk=pk)
            instance.category = validated_data.pop('category')
            # Clear all skills from instance but don't delete
            instance.skills.clear()
            for skill in skills:
                # Returns tuple (instance, created_bool)
                skill_obj, crtd_bool = Skill.objects.get_or_create(**skill)
                skill_obj.skillset_set.add(instance)
        except Exception as e:
            raise e

    def destroy(self, bio_pk, pk):
        return super().destroy(bio_pk, pk)
