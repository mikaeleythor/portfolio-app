from django.http import QueryDict
from django.db.models import QuerySet
from uuid import UUID
from bios.models import Bio, Skillset, Education, Experience, Certificate
from utils.BaseService import BaseService


class BioService(BaseService):
    model = Bio

    def create(self, validated_data: QueryDict) -> UUID:
        return super().create(validated_data)

    def retrieve(self, pk):
        # HACK: Used for debugging
        if pk is None:
            raise KeyError
        return super().retrieve(pk)

    def retrieve_if_published(self, pk):
        # HACK: Used for debugging
        if pk is None:
            raise KeyError
        return super().retrieve_if_published(pk)

    def list_all(self) -> QuerySet:
        return super().list_all()

    def list_published(self) -> QuerySet:
        return super().list_published()

    def update(self, validated_data: QueryDict, pk) -> UUID:
        return super().update(validated_data, pk)

    def destroy(self, pk) -> None:
        return super().destroy(pk)
