from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from bios.serializers import BioSerializer
from bios.services import BioService
from utils.BaseViewSet import BaseViewSet


class BioViewSet(BaseViewSet):
    """
    PeopleViewSet combines the logic for views directly interfacing the
    People model
    """
    service = BioService()
    serializer = BioSerializer
    parser_classes = [JSONParser]

    # NOTE: Implement in BaseViewSet?
    def retrieve(self, request, pk=None) -> Response:
        try:
            if request.user.is_staff:
                instance = self.service.retrieve(pk)
            else:
                instance = self.service.retrieve_if_published(pk)
            return Response(
                data=self.serializer(instance).data,
                status=status.HTTP_200_OK
            )
        except ObjectDoesNotExist as e:
            return Response(data=str(e), status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return Response(data=str(e), status=status.HTTP_204_NO_CONTENT)

    def create(self, request) -> Response:
        return super().create(request)

    def list(self, request) -> Response:
        return super().list(request)

    def update(self, request, pk=None) -> Response:
        return super().update(request, pk)

    def destroy(self, request, pk=None) -> Response:
        return super().destroy(request, pk)
