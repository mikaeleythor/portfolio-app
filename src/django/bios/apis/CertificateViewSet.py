from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser
from utils.permissions import ReadOnly
from bios.serializers import CertificateSerializer
from bios.services.CertificateService import CertificateService
from utils.BaseAttrViewSet import BaseAttrViewSet


class CertificateViewSet(BaseAttrViewSet):

    service = CertificateService()
    serializer = CertificateSerializer
    permission_classes = [IsAuthenticated | ReadOnly]
    parser_classes = [JSONParser]

    def create(self, request, bio_pk=None):
        return super().create(request, bio_pk)

    def list(self, request, bio_pk=None):
        return super().list(request, bio_pk)

    def update(self, request, bio_pk=None, pk=None):
        return super().update(request, bio_pk, pk)

    def destroy(self, request, bio_pk=None, pk=None):
        return super().destroy(request, bio_pk, pk)
