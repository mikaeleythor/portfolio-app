from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import JSONParser
from utils.permissions import ReadOnly
from bios.serializers import SkillsetSerializer
from bios.services import SkillsetService
from utils.BaseAttrViewSet import BaseAttrViewSet


class SkillsetViewSet(BaseAttrViewSet):

    service = SkillsetService()
    serializer = SkillsetSerializer
    permission_classes = [IsAuthenticated | ReadOnly]
    parser_classes = [JSONParser]

    def create(self, request, bio_pk=None):
        return super().create(request, bio_pk)

    def list(self, request, bio_pk=None):
        return super().list(request, bio_pk)

    def update(self, request, bio_pk=None, pk=None):
        return super().update(request, bio_pk, pk)

    def destroy(self, request, bio_pk=None, pk=None):
        return super().destroy(request, bio_pk, pk)
