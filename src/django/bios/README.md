## Bios

The bios app has 10 endpoints for the 12 use cases specified below
Use cases are tested in numbered test-methods in the `tests/` directory

- [ ] Rewrite class diagram
- [ ] Revise endpoint table

```plantuml

package BiosDomain <<Cloud>> {

  class Person {
    name: str
    bio: str
    updateData()
    editExperience()
    editEducation()
    editSkills()
  }
  class Experience {
    company: str
    title: str
    description: str
    period: obj
    updateData()
  }
  class Education {
    school: str
    degree: str
    description: str
    year: int
    updateData()
  }
  class Skills {
    category: str
    items: list
    updateData()
  }

  Person ||--o{ Experience
  Person ||--o{ Skills
  Person ||--o{ Education

}
```

## Endpoints

| Endpoint                           | Methods          | Description                                          |
| ---------------------------------- | ---------------- | ---------------------------------------------------- |
| `bios`                             | `GET,POST`       | Retrieve a list of bios or create a bio              |
| `bios/<bio-id>`                    | `GET,PUT,DELETE` | Retrieve, update or delete a bio                     |
| `bios/<bio-id>/s`                  | `GET,POST`       | Retrieve a list of skillsets or add a skillset       |
| `bios/<bio-id>/s/<skillset-id>`    | `PUT,DELETE`     | Retrieve, edit or delete a skillset                  |
| `bios/<bio-id>/x`                  | `GET,POST`       | Retrieve a list of experience or add an experience   |
| `bios/<bio-id>/x/<experience-id>`  | `PUT,DELETE`     | Retrieve, edit or delete an experience               |
| `bios/<bio-id>/e`                  | `GET,POST`       | Retrieve a list of education or add an education     |
| `bios/<bio-id>/e/<education-id>`   | `PUT,DELETE`     | Retrieve, edit or delete an education                |
| `bios/<bio-id>/c`                  | `GET,POST`       | Retrieve a list of certificates or add a certificate |
| `bios/<bio-id>/c/<certificate-id>` | `PUT,DELETE`     | Retrieve, edit or delete a certificate               |

## Use cases

| #   | Description                                                                | Priority |
| --- | -------------------------------------------------------------------------- | -------- |
| 1   | User can create a bio with name, bio, display-photo                        | A        |
| 2   | User can choose to publish or unpublish existing bio                       | A        |
| 3   | User can update bio attributes                                             | A        |
| 4   | User can delete a bio                                                      | A        |
| 5   | User can add multiple skillsets to a bio                                   | A        |
| 6   | User can edit each skillset for a bio                                      | A        |
| 7   | User can remove a skillset from a bio                                      | A        |
| 8   | User can add a list of education, experience or certificates to a bio      | A        |
| 9   | User can edit a list of education, experience or certificates for a bio    | A        |
| 10  | User can remove a list of education, experience or certificates from a bio | A        |
| 11  | Anyone can retrieve a published bio with all attributes                    | A        |
| 12  | Anyone can retrieve a list of published bios                               | A        |
