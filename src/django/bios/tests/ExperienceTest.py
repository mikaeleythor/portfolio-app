from bios.apis.ExperienceViewSet import ExperienceViewSet
from bios.tests.BioTest import BioTest
from bios.tests.test_data import EXPERIENCE

#################################################
# ############### STATIC DATA ###################
#################################################

POST_DATA = EXPERIENCE['POST']
PUT_DATA = EXPERIENCE['PUT']


class ExperienceTest(BioTest):

    #################################################
    # ############# HELPER METHODS ##################
    #################################################

    def _post_bio(self):
        return super()._post_bio()

    def _post_edu(self, bioID):
        request = self.factory.post(
            path=f'{self.endpoint}{bioID}/Experiences/',
            data=POST_DATA,
            format='json'
        )
        request.user = self.user
        response = ExperienceViewSet.as_view({'post': 'create'})(
            request, bio_pk=bioID
        )
        return response

    def _put_edu(self, bioID: str, eduID: str):
        request = self.factory.put(
            path=f'{self.endpoint}{bioID}/Experiences/{eduID}',
            data=PUT_DATA,
            format='json'
        )
        request.user = self.user
        response = ExperienceViewSet.as_view({'put': 'update'})(
            request, bio_pk=bioID, pk=eduID
        )
        return response

    def _delete_edu(self, bioID: str, eduID: str):
        request = self.factory.delete(
            path=f'{self.endpoint}{bioID}/Experiences/{eduID}',
            format='json'
        )
        request.user = self.user
        response = ExperienceViewSet.as_view({'delete': 'destroy'})(
            request, bio_pk=bioID, pk=eduID
        )
        return response

    #################################################
    # ################### TESTS #####################
    #################################################

    def setUp(self):
        super().setUp()

    def testAddExperience(self):
        """
        USE CASE 8
        User can add a list of education, experience or certificates to a bio
        """
        bioID = self._post_bio().data['id']
        response = self._post_edu(bioID)
        # TEST: status code
        self.assertEqual(
            response.status_code, 201, "Status code should be 201"
        )
        # TEST: response body
        self.assertTrue(
            dict(response.data, **POST_DATA) == response.data,
            "Sent data should be a subset of response body"
        )

    def testUpdateExperience(self):
        """
        USE CASE 9
        User can edit a list of education, experience or certificates for a bio
        """
        bioID = self._post_bio().data['id']
        eduID = self._post_edu(bioID).data['id']
        response = self._put_edu(bioID, eduID)
        # TEST: status code
        self.assertEqual(response.status_code, 202,
                         "Status code should be 202")
        # TEST: response body
        self.assertTrue(
            dict(response.data, **PUT_DATA) == response.data,
            "Sent data should be a subset of response body"
        )

    def testRemoveExperience(self):
        """
        USE CASE 10
        User can remove a list of education, experience or certificates
        from a bio
        """
        bioID = self._post_bio().data['id']
        eduID = self._post_edu(bioID).data['id']
        response = self._delete_edu(bioID, eduID)
        # TEST: status code
        self.assertEqual(response.status_code, 204,
                         "Status code should be 204")
        # TEST: response body
        self.assertEqual(
            response.data, None,
            "Response body should be None"
        )
