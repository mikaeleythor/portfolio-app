djKhaledDescription = """DJ Khaled is an American DJ, record producer,"
    and media personality who was born on November 26, 1975,
    in New Orleans, Louisiana. He began his career as a radio
    host in the 1990s and later transitioned to producing music.
    DJ Khaled is known for his collaborations with other artists,
    motivational lyrics, and his signature catchphrases, such as
    "We the Best" and "Another One." He has released numerous
    albums and singles, many of which have achieved commercial
    success and received critical acclaim. In addition to music,
    DJ Khaled is also a social media influencer, author, and
    entrepreneur. He has become a cultural icon and an influential
    figure in the music industry."""

BIO = {
    'POST': {
        "name": "DJ Khaled",
        "bio": djKhaledDescription,
    },
    'PUT': {
        "name": "DJ Khaled Eyþórsson",
        "bio": djKhaledDescription,
    }
}

SKILLSET = {
    'POST': {
        "category": "DJ Khaled's Skills",
        "skills": [{"name": "DJing"},
                   {"name": "Producing"},
                   {"name": "Collaborating"},
                   {"name": "Business acumen"},
                   {"name": "Social media influence"},
                   {"name": "Public speaking"}
                   ]
    },
    'PUT': {
        "category": "DJ Khaled's Cooking Skills",
        "skills": [{"name": "Sautéeing"},
                   {"name": "Searing"},
                   {"name": "Stirring"},
                   {"name": "Seasoning"},
                   ]
    }
}

EXPERIENCE = {
    'POST': {
        'company': 'Dr. Phillips High School - Orlando, Florida',
        'title': 'Matriculation',
        'description': 'Mafs and stuf',
        'date_start': '1991-01-01',
        'date_end': '1994-01-01'
    },
    'PUT': {
        'company': 'Reykjavik University',
        'title': 'Mechatronics Engineering BSc',
        'description': 'Very hard',
        'date_start': '2019-08-04',
        'date_end': '2023-06-17'
    }
}

EDUCATION = {
    'POST': {
        'school': 'Dr. Phillips High School - Orlando, Florida',
        'degree': 'Matriculation',
        'description': 'Mafs and stuf',
        'date_start': '1991-01-01',
        'date_end': '1994-01-01'
    },
    'PUT': {
        'school': 'Reykjavik University',
        'degree': 'Mechatronics Engineering BSc',
        'description': 'Very hard',
        'date_start': '2019-08-04',
        'date_end': '2023-06-17'
    }

}

CERTIFICATE = {
    'POST': {
        'issuing_organization': 'Dr. Phillips High School - Orlando, Florida',
        'name': 'Matriculation',
        'description': 'Mafs and stuf',
        'date_earned': '1991-01-01',
        'expiration_date': '1994-01-01'
    },
    'PUT': {
        'issuing_organization': 'Reykjavik University',
        'name': 'Mechatronics Engineering BSc',
        'description': 'Very hard',
        'date_earned': '2019-08-04',
        'expiration_date': '2023-06-17'
    }
}
