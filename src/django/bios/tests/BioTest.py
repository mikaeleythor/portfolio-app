from django.contrib.auth.models import User, AnonymousUser
from django.test import TestCase
from rest_framework.test import APIRequestFactory
from bios.apis.BioViewSet import BioViewSet
from bios.apis.SkillsetViewSet import SkillsetViewSet
from bios.apis.EducationViewSet import EducationViewSet
from bios.apis.ExperienceViewSet import ExperienceViewSet
from bios.apis.CertificateViewSet import CertificateViewSet
from create_user import create_test_user
from bios.tests.test_data import BIO, SKILLSET, EDUCATION, EXPERIENCE, \
    CERTIFICATE

#################################################
# ############### STATIC DATA ###################
#################################################

POST_DATA = BIO['POST']
PUT_DATA = BIO['PUT']


class BioTest(TestCase):

    #################################################
    # ############# HELPER METHODS ##################
    #################################################

    def _post_bio(self):
        request = self.factory.post(
            path=self.endpoint,
            data=POST_DATA,
            format='json'
        )
        request.user = self.user
        response = BioViewSet.as_view({'post': 'create'})(request)
        return response

    def _get_bios(self, user):
        request = self.factory.get(
            path=self.endpoint,
        )
        request.user = user
        response = BioViewSet.as_view({'get': 'list'})(request)
        return response

    def _put_bio(self, bioID: str):
        request = self.factory.put(
            path=f'{self.endpoint}{bioID}/',
            data=PUT_DATA,
            format='json'
        )
        request.user = self.user

        response = BioViewSet.as_view(
            {'put': 'update'})(request, pk=bioID)

        return response

    def _delete_bio(self, bioID: str):
        request = self.factory.delete(
            path=f'{self.endpoint}{bioID}/',
            format='json'
        )
        request.user = self.user

        response = BioViewSet.as_view(
            {'delete': 'destroy'})(request, pk=bioID)

        return response

    def _publish_bio(self, bioID: str):
        data = {
            "name": POST_DATA['name'],
            "bio": POST_DATA['bio'],
            "published": True
        }
        request = self.factory.put(
            path=f'{self.endpoint}{bioID}/',
            data=data,
            format='json'
        )
        request.user = self.user

        response = BioViewSet.as_view(
            {'put': 'update'})(request, pk=bioID)

        return response

    # TEST: Should post all attributes
    def _post_bio_with_attrs(self):
        responses = []
        bioID = self._post_bio().data['id']
        for attr in [
            ('skillsets', SKILLSET, SkillsetViewSet),
            ('educations', EDUCATION, EducationViewSet),
            ('experiences', EXPERIENCE, ExperienceViewSet),
            ('certificates', CERTIFICATE, CertificateViewSet),
        ]:
            request = self.factory.post(
                path=f'{self.endpoint}{bioID}/{attr[0]}',
                data=attr[1]['POST'],
                format='json'
            )
            request.user = self.user

            # print(f'posting data to {attr[0]}:\n{attr[1]["POST"]}')
            responses.append(
                attr[2].as_view({'post': 'create'})(
                    request, bio_pk=bioID
                )
            )
        return bioID, responses

    def _get_bio(self, bioID, user):
        request = self.factory.get(
            path=f'{self.endpoint}{bioID}',
            format='json'
        )
        request.user = user
        response = BioViewSet.as_view({'get': 'retrieve'})(
            request, pk=bioID
        )
        return response

    #################################################
    # ################### TESTS #####################
    #################################################

    def setUp(self):
        create_test_user()
        self.factory = APIRequestFactory()
        self.user = User.objects.get(username='apitestuser')
        self.anonymous = AnonymousUser()
        self.endpoint = 'bios/'

    def testCreateBio(self):
        """
        USE CASE 1

        User can create a bio with name, bio, display-photo
        """
        response = self._post_bio()
        # TEST: status code
        self.assertEqual(response.status_code, 201,
                         f"Status code should be 201, error: {response.data}")

    def testUpdateBio(self):
        """
        USE CASE 2

        User can choose to publish or unpublish existing bio

        USE CASE 3

        User can update bio attributes
        """
        bioID = self._post_bio().data['id']
        response = self._put_bio(bioID)
        # TEST: status code
        self.assertEqual(response.status_code, 202,
                         "Status code should be 202")

    def testDestroyBio(self):
        """
        USE CASE 4

        User can delete a bio
        """
        bioID = self._post_bio().data['id']
        newResponse = self._delete_bio(bioID)
        # TEST: status code
        self.assertEqual(newResponse.status_code, 204,
                         f"Status code should be 204: {newResponse.data}")

    def testRetrieveBio(self):
        """
        USE CASE 12

        Anyone can retrieve a published bio with all attrs
        """
        bioID, responses = self._post_bio_with_attrs()
        self._publish_bio(bioID)
        response = self._get_bio(bioID, self.anonymous)

        # TEST: status code
        self.assertEqual(response.status_code, 200,
                         f"Status code should be 200: {response.data}")

        # TEST: response body
        self.assertTrue(
            dict(response.data['education'][0],
                 **EDUCATION['POST']) == response.data['education'][0],
            "Sent education data should be a subset of response body"
        )
        self.assertTrue(
            dict(response.data['experience'][0],
                 **EXPERIENCE['POST']) == response.data['experience'][0],
            "Sent experience data should be a subset of response body"
        )
        self.assertTrue(
            dict(response.data['certifications'][0],
                 **CERTIFICATE['POST']) == response.data['certifications'][0],
            "Sent certification data should be a subset of response body"
        )

        self.assertEqual(response.data['skillsets'][0]
                         ['category'], SKILLSET['POST']['category'],
                         "Sent and received category should be the same")
        res_skills = response.data['skillsets'][0]['skills']
        self.assertEqual(len(res_skills), len(SKILLSET['POST']['skills']),
                         "Number of skills should be the same")
        for skill in res_skills:
            self.assertTrue(skill in SKILLSET['POST']['skills'],
                            "Sent skillset should include all received skills")

    def testRetrieveBioAsAnonymous(self):
        """
        USE CASE 12

        Anyone can retrieve a list of published bios
        """
        bioID, responses = self._post_bio_with_attrs()
        response = self._get_bio(bioID, self.anonymous)
        self.assertEqual(response.status_code, 404,
                         "Bio not found should return 404")
        self.assertEqual(response.data, "Bio matching query does not exist.")
        response = self._get_bios(self.anonymous)
        self.assertEqual(len(response.data), 0,
                         "No published bios should return empty list")
        self.assertEqual(response.status_code, 200,
                         "Empty response but successful request")
        response = self._publish_bio(bioID)
        response = self._get_bios(self.anonymous)
        self.assertEqual(len(response.data), 1,
                         "One published bio should return list of length 1")
