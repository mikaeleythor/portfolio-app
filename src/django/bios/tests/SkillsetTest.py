from bios.apis import SkillsetViewSet
from bios.tests.BioTest import BioTest
from bios.tests.test_data import SKILLSET

#################################################
# ############### STATIC DATA ###################
#################################################

POST_DATA = SKILLSET['POST']
PUT_DATA = SKILLSET['PUT']


class SkillsetTest(BioTest):

    #################################################
    # ############# HELPER METHODS ##################
    #################################################

    def _post_bio(self):
        return super()._post_bio()

    def _post_skillset(self, bioID, post_data):
        request = self.factory.post(
            path=f'{self.endpoint}{bioID}/skillsets/',
            data=post_data,
            format='json'
        )
        request.user = self.user

        response = SkillsetViewSet.as_view({'post': 'create'})(
            request, bio_pk=bioID)

        return response

    def _put_skillset(self, bioID: str, skillsetID: str):
        request = self.factory.put(
            path=f'{self.endpoint}{bioID}/skillsets/{skillsetID}',
            data=PUT_DATA,
            format='json'
        )
        request.user = self.user

        response = SkillsetViewSet.as_view({'put': 'update'})(
            request, bio_pk=bioID, pk=skillsetID
        )

        return response

    def _delete_skillset(self, bioID: str, skillsetID: str):
        request = self.factory.delete(
            path=f'{self.endpoint}{bioID}/',
            format='json'
        )
        request.user = self.user

        response = SkillsetViewSet.as_view(
            {'delete': 'destroy'})(request, bio_pk=bioID, pk=skillsetID)

        return response

    #################################################
    # ################### TESTS #####################
    #################################################

    def setUp(self):
        super().setUp()

    def testAddSkillset(self):
        """
        USE CASE 5

        User can add multiple skillsets to a bio
        """

        response = self._post_bio()
        bioID = response.data['id']
        for data in [POST_DATA, PUT_DATA]:
            response = self._post_skillset(bioID, data)
            self.assertEqual(response.status_code, 201,
                             f"Status code should be 201: {response.data}")

    def testUpdateSkillset(self):
        """
        USE CASE 6

        User can edit each skillset for a bio
        """
        bioID = self._post_bio().data['id']

        skillsetID = self._post_skillset(bioID, POST_DATA).data['id']

        response = self._put_skillset(bioID, skillsetID)

        self.assertEqual(response.status_code, 202,
                         "Status code should be 202")

    def testDestroySkillset(self):
        """
        USE CASE 7

        User can remove a skillset from a bio
        """
        bioID = self._post_bio().data['id']

        skillsetID = self._post_skillset(bioID, POST_DATA).data['id']

        response = self._delete_skillset(bioID, skillsetID)
        self.assertEqual(response.status_code, 204,
                         f"Status code should be 204: {response.data}")
