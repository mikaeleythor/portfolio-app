from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.permissions import IsAuthenticated
from utils.permissions import ReadOnly


class BaseAttrViewSet(ViewSet):
    """
    This ViewSet is the result of refactoring ViewSets for attributes of the
    base models of the Projects app and Bios app, such as:

    ReportViewSet, PhotoViewSet, EducationViewSet, SkillsetViewSet

    and is a generice implementation of a ViewSet for attributes of a
    BaseViewSet such as ProjectViewSet or BioViewSet
    """

    # NOTE: This is hardcoded into this class because of the implementation
    # of service.list_all() and service.list_published()
    permission_classes = [IsAuthenticated | ReadOnly]

    # NOTE: The following should be implemented in child classes:
    # service = SomeService()
    # serializer = SomeSerializer

    # NOTE: These can be implemented if default classes are not sufficient
    # renderer_classes = [JPEGRenderer, PNGRenderer]
    # parser_classes = [MultiPartParser, FormParser, JSONParser]

    # WARNING: PATCH is not implemented, this variable needs to
    # be redefined in child class if PATCH is implemented there
    http_method_names = ['get', 'post', 'put', 'delete']

    # TODO: What could go wrong in this endpoint needs to be
    # tested and appropriate exception handling implemented
    def create(self, request, base_pk=None) -> Response:

        # Validate request data with a serializer defined as a class variable
        serializer = self.serializer(data=request.data)

        if serializer.is_valid():
            try:
                new_instance = self.service.create(
                    base_pk, serializer.validated_data)
                return Response(
                    data=self.serializer(new_instance).data,
                    status=status.HTTP_201_CREATED
                )

            except ObjectDoesNotExist as e:
                return Response(data=str(e), status=status.HTTP_404_NOT_FOUND)

            except Exception as e:
                return Response(data=e,
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    # TODO: What could go wrong in this endpoint needs to be
    # tested and appropriate exception handling implemented
    def list(self, request, base_pk=None) -> Response:
        try:
            if request.user.is_staff:
                queryset = self.service.list(base_pk)
            else:
                queryset = self.service.list_if_published(base_pk)

            return Response(
                [self.serializer(query).data for query in queryset]
            )

        except ObjectDoesNotExist as e:
            return Response(data=str(e), status=status.HTTP_404_NOT_FOUND)

    # TODO: What could go wrong in this endpoint needs to be
    # tested and appropriate exception handling implemented
    def update(self, request, base_pk=None, pk=None) -> Response:
        serializer = self.serializer(data=request.data)

        if serializer.is_valid():
            try:
                updated_instance = self.service.update(
                    base_pk, pk, serializer.validated_data
                )
                return Response(
                    data=self.serializer(updated_instance).data,
                    status=status.HTTP_202_ACCEPTED
                )

            except ObjectDoesNotExist as e:
                return Response(data=str(e), status=status.HTTP_404_NOT_FOUND)

        else:
            return Response(
                data=serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )

    # TODO: What could go wrong in this endpoint needs to be
    # tested and appropriate exception handling implemented
    def destroy(self, request, base_pk=None, pk=None) -> Response:
        try:
            self.service.destroy(base_pk, pk)
            return Response(status=status.HTTP_204_NO_CONTENT)

        except ObjectDoesNotExist as e:
            return Response(data=str(e), status=status.HTTP_404_NOT_FOUND)

        except Exception as e:
            return Response(data=e, status=status.HTTP_400_BAD_REQUEST)
