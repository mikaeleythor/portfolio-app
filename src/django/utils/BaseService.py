from uuid import UUID
from django.http import QueryDict
from django.db.models import QuerySet


class BaseService:
    """
    This Service is the result of refactoring base Services of
    the Projects app and Bios app, i.e. ProjectService & BioService
    """

    # NOTE: The following should be implemented in child classes:
    # model = SomeModel

    def retrieve(self, pk: UUID) -> QueryDict:
        return self.model.objects.get(id=pk)

    def retrieve_if_published(self, pk: UUID) -> QuerySet:
        return self.model.objects.get(id=pk, published=True)

    def create(self, validated_data: QueryDict) -> QuerySet:
        return self.model.objects.create(**validated_data)

    def list_all(self) -> QuerySet:
        return self.model.objects.all()

    def list_published(self) -> QuerySet:
        return self.model.objects.filter(published=True)

    def update(self, validated_data: QueryDict, pk: UUID) -> QuerySet:
        if self.model.objects.get(id=pk):
            self.model.objects.filter(id=pk).update(**validated_data)
            return self.model.objects.get(id=pk)
        else:
            raise self.model.DoesNotExist()

    def destroy(self, pk: UUID):
        if self.model.objects.get(id=pk):
            self.model.objects.get(id=pk).delete()
        else:
            raise self.model.DoesNotExist()
