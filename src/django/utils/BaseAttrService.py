from uuid import UUID
from django.http import QueryDict


class BaseAttrService():
    """
    This Service is the result of refactoring base attr Services of
    the Projects app and Bios app, i.e. ReportService & SkillsetService
    """

    # NOTE: The following should be implemented in child classes:
    # model = SomeModel

    def create(self, base_pk: UUID, validated_data: QueryDict) -> UUID:
        try:
            base_instance = self.base.objects.get(id=base_pk)
            validated_data[self.basename] = base_instance
            return self.model.objects.create(**validated_data)

        # TEST: Will there occur a KeyError here?
        except KeyError:
            raise self.base.DoesNotExist()

    def list(self, base_pk: UUID) -> list:
        try:
            base_instance = self.base.objects.get(id=base_pk)
            base_attr = self.model.objects.filter(
                **{self.basename: base_instance})

            # WARNING: This sorting is only used on Project Report Items
            return sorted(list(base_attr), key=lambda item: item.num)

        except KeyError:
            raise self.base.DoesNotExist()

    def list_if_published(self, base_pk: UUID, validated_data: QueryDict) -> list:
        try:
            base_instance = self.base.objects.get(id=base_pk, published=True)
            base_attr = self.model.objects.filter(
                **{self.basename: base_instance})
            return sorted(list(base_attr), key=lambda item: item.num)

        except KeyError:
            raise self.base.DoesNotExist()

    def update(self, base_pk: UUID, pk: int, validated_data: QueryDict):
        if self.base.objects.get(id=base_pk):
            if self.model.objects.get(id=pk):
                self.model.objects.filter(id=pk).update(**validated_data)
                return self.model.objects.get(id=pk)
            else:
                raise self.model.DoesNotExist()
        else:
            raise self.base.DoesNotExist()

    def destroy(self, base_pk: UUID, pk: int) -> None:
        if self.base.objects.get(id=base_pk):
            if self.model.objects.get(id=pk):
                self.model.objects.get(id=pk).delete()
            else:
                raise self.model.DoesNotExist()
        else:
            raise self.base.DoesNotExist()
