from django.contrib.auth import get_user_model
# This script is used for testing in entrypoint scripts

UserModel = get_user_model()

if UserModel.objects.filter(username='apitestuser').exists():
    user = UserModel.objects.get(username='apitestuser')
    user.delete()
