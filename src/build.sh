#!/bin/bash

NAME="resuman"

if [[ -n "$1" ]]; then
  NAME="$NAME:$1"
fi

docker build -f ./django/Dockerfile.prod -t "$NAME" ./django
